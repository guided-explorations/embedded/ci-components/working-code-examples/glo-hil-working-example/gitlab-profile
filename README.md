## Device Cloud Pool Manager for GitLab Lightweight Orchestration of Hardware In the Loop Gateways for CI

## Managing HIL Gateway Runners

Each HIL Gateway Runner has a variable in the group or project which is manaing the pool.

If you have appropriate permissions you will be able to view and update the variables [HERE](https://gitlab.com/groups/guided-explorations/embedded/ci-components/working-code-examples/glo-hil-working-example/-/settings/ci_cd)

The variable name matches the runner tag used to route to the HIL GW instance.

Variable States:
- The Device Cloud Component toggles runner allocation variables between "ONLINE_AND_AVAILABLE" and "INUSEBY_{CI-PIPELINE_ID}_YYYYMMDD-HHMMSS"
- Change a runner varible to "DRAINING-BY-GitLabID-{DATETIME}" to prevent it from being allocated after the current run is done, GitLabID and DATETIME so everyone knows who and when
- Change a runner variable to "OFFLINED-BY-GitLabID-{DATETIME}" to prevent it from being allocated.
- Any text other than "ONLINE_AND_AVAILABLE" prevents reallocation - so you can freely describe other states as well, GitLabID and {DATETIME} are always a good idea.